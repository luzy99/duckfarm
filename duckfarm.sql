SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- 1 Table structure for room
-- ------------------
CREATE TABLE IF NOT EXISTS `room`  (
  `roomid` int(9) DEFAULT 0,
  `roomname` varchar(255) NULL,
  `userCount` int(9) DEFAULT 0,
  `roomMaxNum` int(9) DEFAULT 0,
  `userid` int(9) NOT NULL,
  `isPrivate` int(1) DEFAULT 0,
  `jointime` timestamp(0) NOT NULL,
	foreign key(userid) references user(userid),
  PRIMARY KEY (`userid`)
)CHARSET=utf8;

-- ----------------------------
-- 2 Table structure for user
-- ------------------
CREATE TABLE IF NOT EXISTS `user`  (
  `userid` int(9) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `headImageLink` varchar(255) NOT NULL,
  `fansNum` int(9) DEFAULT 0,
  `careNum` int(9) DEFAULT 0,
  `groupNum` int(9) DEFAULT 0,
  `power` int(9) DEFAULT 0,
  `credit` int(9) DEFAULT 0,
  `creditgotten` int(9) DEFAULT 0,
  `recoverTime` timestamp NULL,
  `status` int(1) DEFAULT 0,
  `isAdministator` int(1) DEFAULT 0,
  PRIMARY KEY (`userid`)
)CHARSET=utf8 AUTO_INCREMENT=1000000;

-- ----------------------------
-- 3 Table structure for work
-- -----------------------
CREATE TABLE IF NOT EXISTS `work`  (
  `workid` int(9) NOT NULL AUTO_INCREMENT,
  `text` varchar(2555) NULL,
  `title` varchar(255) NULL,
  `imageLink` varchar(2555) NULL,
  `releaseTime` timestamp NULL,
  `userid` int(9) NOT NULL,
  `attitudeNum` int(9) DEFAULT 0,
  `commentNum` int(9) DEFAULT 0,
  `getPaid` int(9) DEFAULT 0,
  `groupid` int(9) NOT NULL,
  `status` int(1) DEFAULT 0,
  	foreign key(userid) references user(userid),
	foreign key(groupid) references community(groupid),
  PRIMARY KEY (`workid`)
)CHARSET=utf8 AUTO_INCREMENT = 1;

-- ----------------------------
-- 4 Table structure for comment
-- ----------------------------
CREATE TABLE IF NOT EXISTS `comment`  (
	`commentid` int(9) NOT NULL AUTO_INCREMENT,
	`content` varchar(2555) NULL,
	`commentparentid` varchar(255) NOT NULL,
	`parentuserid` int(9) NOT NULL,
	`userid` int(9) NOT NULL,
	`releasetime` timestamp(0) NULL,
	`rootknot` int(9) NOT NULL,
	foreign key(userid) references user(userid),
	foreign key(parentuserid) references user(userid),
	foreign key(rootknot) references work(workid),
	PRIMARY KEY (`commentid`)
	)CHARSET=utf8 AUTO_INCREMENT=7000000;
	


-- ----------------------------
-- 5 Table structure for tagreflect
-- ----------------------------
CREATE TABLE IF NOT EXISTS `tagreflect`  (
	`tagid` int(9) NOT NULL AUTO_INCREMENT,
	`tag` varchar(255) NOT NULL,
	PRIMARY KEY (`tagid`)
	)CHARSET=utf8 AUTO_INCREMENT=1;
	
-- ----------------------------
-- 6 Table structure for tagwork
-- ----------------------------
CREATE TABLE IF NOT EXISTS `tagwork`  (
	`tagworkid` int(9) NOT NULL AUTO_INCREMENT,
	`tagid` int(9) NOT NULL,
	`workid` int(9) NOT NULL,
	PRIMARY KEY (`tagworkid`),
	foreign key(tagid) references tagreflect(tagid),
	foreign key(workid) references work(workid)
	)CHARSET=utf8 AUTO_INCREMENT=1;




-- ----------------------------
-- 7.Table structure for todolist
-- ----------------------------
CREATE TABLE IF NOT EXISTS `todolist`  (
	`todoid` int(9) NOT NULL AUTO_INCREMENT,
	`title` varchar(255) NOT NULL,
	`content` varchar(2555) NULL,
	`userid` int(9) NOT NULL,
	`createtime` timestamp(0) NULL,
	`endtime` timestamp(0) NULL,
	`finishtime` timestamp(0) NULL,
	`isfinished` int(1) NULL,
	`alarmtime` timestamp(0) NULL,
	foreign key(userid) references user(userid),
	PRIMARY KEY (`todoid`)
	)CHARSET=utf8 AUTO_INCREMENT=1;


-- ----------------------------
-- 8.Table structure for longplan
-- ----------------------------
CREATE TABLE IF NOT EXISTS `longplan`  (
	`planid` int(9) NOT NULL AUTO_INCREMENT,
	`title` varchar(255) NOT NULL,
	`targettime` int(9) NULL DEFAULT 0,
	`studytime` int(9) NULL DEFAULT 0,
	`userid` int(9) NOT NULL,
	`createtime` timestamp(0) NULL,
	`endtime` timestamp(0) NULL,
	`finishtime` timestamp(0) NULL,
	`isfinished` int(1) NULL DEFAULT 0,
	`alarmtime` timestamp(0) NULL,
	`content` varchar(2555) NULL,
	`focustime` int(9) DEFAULT 0,
	`relaxtime` int(9) DEFAULT 0,
    `circulate` int(1) DEFAULT 0,
    `blackhouse` int(1) DEFAULT 0,
	foreign key(userid) references user(userid),
	PRIMARY KEY (`planid`)
	)CHARSET=utf8 AUTO_INCREMENT=1;
	
-- ----------------------------
-- 9 Table structure for clock
-- ----------------------------
CREATE TABLE IF NOT EXISTS `clock`  (
	`clockid` int(9) NOT NULL AUTO_INCREMENT,
	`userid` int(9) NOT NULL,
	`focustime` int(9) DEFAULT 0,
	`relaxtime` int(9) DEFAULT 0,
    `title` varchar(255) NOT NULL,
    `circulate` int(1) DEFAULT 0,
    `blackhouse` int(1) DEFAULT 0,
	foreign key(userid) references user(userid),
    PRIMARY KEY (`clockid`)
	)CHARSET=utf8 AUTO_INCREMENT=1;
	
-- ----------------------------
-- 10 Table structure for clockrecord
-- ----------------------------
CREATE TABLE IF NOT EXISTS `studyrecord`  (
	`recordid` int(9) NOT NULL AUTO_INCREMENT,
	`userid` int(9) NOT NULL,
	`runtime` int(9) DEFAULT 0,
    `target` varchar(255) NOT NULL,
    `starttime` timestamp NULL,
    `iscompleted` int(1) DEFAULT 0,
    `failreason` varchar(255) NULL,
	`type` int(1) NOT NULL,
	foreign key(userid) references user(userid),
    PRIMARY KEY (`recordid`)
	)CHARSET=utf8 AUTO_INCREMENT=1;


-- ----------------------------
-- 11 Table structure for follow
-- ----------------------------
CREATE TABLE IF NOT EXISTS `follow`  (
	`followid` int(9) NOT NULL AUTO_INCREMENT,
	`userid` int(9) NOT NULL,
	`follower` int(9) NOT NULL,
	foreign key(userid) references user(userid),
	foreign key(follower) references user(userid),
    PRIMARY KEY (`followid`)
	)CHARSET=utf8 AUTO_INCREMENT=1;
	
	
-- ----------------------------
-- 12 Table structure for grouprelation
-- ----------------------------
CREATE TABLE IF NOT EXISTS `grouprelation`  (
	`relationid` int(9) NOT NULL AUTO_INCREMENT,
	`userid` int(9) NOT NULL,
	`groupid` int(9) NOT NULL,
	foreign key(userid) references user(userid),
	foreign key(groupid) references community(groupid),
    PRIMARY KEY (`relationid`)
	)CHARSET=utf8 AUTO_INCREMENT=1;
	


-- ----------------------------
-- 13 Table structure for event
-- ----------------------------
CREATE TABLE IF NOT EXISTS `event`  (
	`eid` int(9) NOT NULL AUTO_INCREMENT,
    `eventtype` int(9) NOT NULL, 
	`eventid` int(9) NOT NULL,
	`status` int(1) DEFAULT 0,
    PRIMARY KEY (`eid`)
	)CHARSET=utf8 AUTO_INCREMENT=1;
	
-- ----------------------------
-- 14 Table structure for group
-- ----------------------------
CREATE TABLE IF NOT EXISTS `community`  (
	`groupid` int(9) NOT NULL AUTO_INCREMENT,
	`userid` int(9) NOT NULL,
    `groupname` varchar(255) NOT NULL,
    `createtime` timestamp NOT NULL,
    `status` int(1) DEFAULT 0,
    `description` varchar(255) NULL,
    `backgroundimage` varchar(255) NULL,
	foreign key(userid) references user(userid),
    PRIMARY KEY (`groupid`)
	)CHARSET=utf8 AUTO_INCREMENT=1;
	
	
-- ----------------------------
-- 15 Table structure for workrelation
-- ----------------------------
CREATE TABLE IF NOT EXISTS `workrelation`  (
	`workrelationid` int(9) NOT NULL AUTO_INCREMENT,
	`userid` int(9) NOT NULL,
	`workid` int(9) NOT NULL,
	foreign key(userid) references user(userid),
	foreign key(workid) references work(workid),
    PRIMARY KEY (`workrelationid`)
	)CHARSET=utf8 AUTO_INCREMENT=1;
	
	
-- ----------------------------
-- 16 Table structure for likerelation
-- ----------------------------
CREATE TABLE IF NOT EXISTS `likerelation`  (
	`likeid` int(9) NOT NULL AUTO_INCREMENT,
	`userid` int(9) NOT NULL,
	`workid` int(9) NOT NULL,
	foreign key(userid) references user(userid),
	foreign key(workid) references work(workid),
    PRIMARY KEY (`likeid`)
	)CHARSET=utf8 AUTO_INCREMENT=1;
	
-- ----------------------------
-- 17 Table structure for payrelation
-- ----------------------------
CREATE TABLE IF NOT EXISTS `payrelation`  (
	`payid` int(9) NOT NULL AUTO_INCREMENT,
	`userid` int(9) NOT NULL,
    `workid` int(9) NOT NULL,
	foreign key(userid) references user(userid),
	foreign key(workid) references work(workid),
	PRIMARY KEY (`payid`)
	)CHARSET=utf8 AUTO_INCREMENT=1;
	
SET FOREIGN_KEY_CHECKS=1;