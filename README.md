# DuckFarm ——学习鸭 Django 后端
## 项目启动

1. 安装依赖

   ```sh
   pip install django
   pip install django-cors-headers
   ```

2. 目录结构

   ```shell
   │  .gitignore
   │  manage.py
   │  readme.md
   │
   ├─DuckFarm
   │  │  settings.py	（配置数据库）
   │  │  urls.py
   │  └─ wsgi.py
   │
   ├─media
   │  ├─headimg (用户头像)
   │  │  
   │  ├─groupimg  （圈子背景）
   │  │  
   │  └─postimg   （帖子图片）
   │
   └─duckserver
       │  admin.py
       │  apps.py
       │  models.py
       │  tests.py
       │  urls.py		（设置路由）
       │  views.py		（相应请求）
       │
       ├─service	（此目录下为服务函数）
       │  │  user.py
   
   ```

   

3. 导入数据库`duckfarm.sql`

   > 已采用model直接迁移方式，无需手动导入数据库

4. 迁移数据库

   ```shell
   python manage.py makemigrations duckserver
   python manage.py migrate duckserver
   ```

5. 启动服务

   ```sh
   python manage.py runserver
   ```

