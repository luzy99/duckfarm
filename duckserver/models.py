# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Clock(models.Model):
    clockid = models.AutoField(primary_key=True)
    userid = models.ForeignKey('User', models.DO_NOTHING, db_column='userid')
    focustime = models.IntegerField(blank=True, null=True)
    relaxtime = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=255)
    circulate = models.IntegerField(blank=True, null=True)
    blackhouse = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'clock'


class Studyrecord(models.Model):
    recordid = models.AutoField(primary_key=True)
    userid = models.ForeignKey('User', models.DO_NOTHING, db_column='userid')
    runtime = models.IntegerField(blank=True, null=True)
    target = models.CharField(max_length=255)
    starttime = models.DateTimeField(blank=True, null=True)
    iscompleted = models.IntegerField(blank=True, null=True)
    failreason = models.CharField(max_length=255, blank=True, null=True)
    type = models.IntegerField()
    
    class Meta:
        managed = True
        db_table = 'studyrecord'


class Comment(models.Model):
    commentid = models.AutoField(primary_key=True)
    content = models.CharField(max_length=2555, blank=True, null=True)
    commentparentid = models.CharField(max_length=255)
    parentuserid = models.ForeignKey(
        'User', models.DO_NOTHING, db_column='parentuserid', related_name='parentuserid')
    userid = models.ForeignKey('User', models.DO_NOTHING, db_column='userid')
    releasetime = models.DateTimeField(
        blank=True, null=True, auto_now_add=True)
    rootknot = models.ForeignKey(
        'Work', models.DO_NOTHING, db_column='rootknot')

    class Meta:
        managed = True
        db_table = 'comment'


# 圈子头像名称
def group_backgroundimage(instance, filename):
    ext = filename.split('.')[-1]
    return 'groupimg/group_{0}.{1}'.format(instance.groupid, ext)


class Community(models.Model):
    groupid = models.AutoField(primary_key=True)
    userid = models.ForeignKey('User', models.DO_NOTHING, db_column='userid')
    groupname = models.CharField(max_length=255)
    createtime = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(blank=True, null=True, default=0)
    description = models.CharField(max_length=255, blank=True, null=True)
    backgroundimage = models.ImageField(
        upload_to=group_backgroundimage, default="groupimg/default.jpg")
    announcement = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'community'


class Event(models.Model):
    eid = models.AutoField(primary_key=True)
    eventtype = models.IntegerField()
    eventid = models.IntegerField()
    status = models.IntegerField(blank=True, null=True, default=0)
    edittime = models.DateTimeField(auto_now=True)  # 修改时间
    reason = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'event'


class Follow(models.Model):
    followid = models.AutoField(primary_key=True)
    userid = models.ForeignKey('User', models.DO_NOTHING, db_column='userid')
    follower = models.ForeignKey(
        'User', models.DO_NOTHING, db_column='follower', related_name='follower')
    createtime = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'follow'


class Grouprelation(models.Model):
    relationid = models.AutoField(primary_key=True)
    userid = models.ForeignKey('User', models.DO_NOTHING, db_column='userid')
    groupid = models.ForeignKey(
        Community, models.DO_NOTHING, db_column='groupid')
    createtime = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'grouprelation'


class Likerelation(models.Model):
    likeid = models.AutoField(primary_key=True)
    userid = models.ForeignKey('User', models.DO_NOTHING, db_column='userid')
    workid = models.ForeignKey('Work', models.DO_NOTHING, db_column='workid')
    createtime = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'likerelation'


class Longplan(models.Model):
    planid = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    targettime = models.IntegerField(blank=True, null=True)
    studytime = models.IntegerField(blank=True, null=True, default=0)
    userid = models.ForeignKey('User', models.DO_NOTHING, db_column='userid')
    createtime = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    endtime = models.DateTimeField(blank=True, null=True)
    finishtime = models.DateTimeField(blank=True, null=True)
    isfinished = models.IntegerField(blank=True, null=True, default=0)
    alarmtime = models.DateTimeField(blank=True, null=True)
    content = models.CharField(max_length=2555, blank=True, null=True)
    focustime = models.IntegerField(blank=True, null=True)
    relaxtime = models.IntegerField(blank=True, null=True)
    circulate = models.IntegerField(blank=True, null=True)
    blackhouse = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'longplan'


class Payrelation(models.Model):
    payid = models.AutoField(primary_key=True)
    userid = models.ForeignKey('User', models.DO_NOTHING, db_column='userid')
    workid = models.ForeignKey('Work', models.DO_NOTHING, db_column='workid')
    createtime = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'payrelation'


class Room(models.Model):
    roomid = models.IntegerField(blank=True, null=True)
    roomname = models.CharField(max_length=255, blank=True, null=True)
    # Field name made lowercase.
    usercount = models.IntegerField(
        db_column='userCount', blank=True, null=True, default=1)
    # Field name made lowercase.
    roommaxnum = models.IntegerField(
        db_column='roomMaxNum', blank=True, null=True, default=100)
    userid = models.ForeignKey(
        'User', models.DO_NOTHING, db_column='userid', primary_key=True)
    # Field name made lowercase.
    isprivate = models.IntegerField(
        db_column='isPrivate', blank=True, null=True, default=0)
    jointime = models.DateTimeField(auto_now_add=True, null=True)

    class Meta:
        managed = True
        db_table = 'room'


class Tagreflect(models.Model):
    tagid = models.AutoField(primary_key=True)
    tag = models.CharField(max_length=255)
    createtime = models.DateTimeField(auto_now_add=True, null=True)

    class Meta:
        managed = True
        db_table = 'tagreflect'


class Tagwork(models.Model):
    tagworkid = models.AutoField(primary_key=True)
    tagid = models.ForeignKey(Tagreflect, models.DO_NOTHING, db_column='tagid')
    workid = models.ForeignKey('Work', models.DO_NOTHING, db_column='workid')

    class Meta:
        managed = True
        db_table = 'tagwork'


class Todolist(models.Model):
    todoid = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    content = models.CharField(max_length=2555, blank=True, null=True)
    userid = models.ForeignKey('User', models.DO_NOTHING, db_column='userid')
    createtime = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    endtime = models.DateTimeField(blank=True, null=True)
    finishtime = models.DateTimeField(blank=True, null=True)
    isfinished = models.IntegerField(blank=True, null=True, default=0)
    alarmtime = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'todolist'

# 用户头像名称


def user_head_img(instance, filename):
    ext = filename.split('.')[-1]
    return 'headimg/user_{0}.{1}'.format(instance.userid, ext)


class User(models.Model):
    userid = models.AutoField(primary_key=True)
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    email = models.CharField(max_length=255, null=True)
    # Field name made lowercase.
    # headImagelink = models.CharField(db_column='headImageLink', max_length=255)
    headImage = models.ImageField(
        upload_to=user_head_img, default="headimg/default.jpg")
    # Field name made lowercase.
    fansNum = models.IntegerField(
        db_column='fansNum', blank=True, null=True, default=0)
    # Field name made lowercase.
    careNum = models.IntegerField(
        db_column='careNum', blank=True, null=True, default=0)
    # Field name made lowercase.
    groupNum = models.IntegerField(
        db_column='groupNum', blank=True, null=True, default=0)
    power = models.FloatField(blank=True, null=True, default=100)
    credit = models.IntegerField(blank=True, null=True, default=0)
    creditgotten = models.IntegerField(blank=True, null=True, default=0)
    # Field name made lowercase.
    recoverTime = models.DateTimeField(auto_now_add=True, db_column='recoverTime', blank=True, null=True)
    status = models.IntegerField(blank=True, null=True, default=0)
    # Field name made lowercase.
    isadministator = models.IntegerField(
        db_column='isAdministator', blank=True, null=True, default=0)
    lastMsgTime = models.DateTimeField(
        auto_now_add=True, blank=True, null=True)  # 上次获取消息的时间
    sex = models.IntegerField(
        db_column='sex', blank=True, null=True, default=0)

    followweight = models.FloatField(blank=True, null=True, default=0.2)
    groupweight = models.FloatField(blank=True, null=True, default=0.2)
    heatweight = models.FloatField(blank=True, null=True, default=0.2)
    paidweight = models.FloatField(blank=True, null=True, default=0.1)
    tagweight = models.FloatField(blank=True, null=True, default=0.3)
    class Meta:
        managed = True
        db_table = 'user'


class Work(models.Model):
    workid = models.AutoField(primary_key=True)
    text = models.CharField(max_length=2555, blank=True, null=True)
    # Field name made lowercase.
    imagenum = models.IntegerField(blank=True, default=0)
    # Field name made lowercase.
    releasetime = models.DateTimeField(
        auto_now_add=True, db_column='releaseTime', blank=True, null=True)
    userid = models.ForeignKey(User, models.DO_NOTHING, db_column='userid')
    # Field name made lowercase.
    attitudenum = models.IntegerField(
        db_column='attitudeNum', blank=True, null=True, default=0)
    # Field name made lowercase.
    commentnum = models.IntegerField(
        db_column='commentNum', blank=True, null=True, default=0)
    # Field name made lowercase.
    getpaid = models.IntegerField(
        db_column='getPaid', blank=True, null=True, default=0)
    groupid = models.ForeignKey(
        Community, models.DO_NOTHING, db_column='groupid')
    status = models.IntegerField(blank=True, null=True, default=0)
    title = models.CharField(max_length=255, blank=True, null=True, default="")
    sharenum = models.IntegerField(
        db_column='shareNum', blank=True, null=True, default=0)

    class Meta:
        managed = True
        db_table = 'work'


# 帖子图片名称
def work_img(instance, filename):
    ext = filename.split('.')[-1]
    return 'postimg/work_{0}.{1}'.format(instance.workid.workid, ext)


class WorkImg(models.Model):
    workimgid = models.AutoField(primary_key=True)
    workid = models.ForeignKey(Work, models.DO_NOTHING, db_column='workid')
    image = models.ImageField(upload_to=work_img,
                              default="postimg/default.jpg")

    class Meta:
        managed = True
        db_table = 'workimg'


class EmailVerify(models.Model):
    email = models.CharField(primary_key=True, max_length=255)
    verifyCode = models.IntegerField(null=True)
    createtime = models.DateTimeField(auto_now=True, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'emailverify'
