from duckserver.models import User
from duckserver.models import Clock, Longplan
from duckserver.service import todolist
from duckserver.service import longplan
import time


# 创建番茄钟
def createClock(userid, title, focustime, relaxtime, circulate, blackhouse):
    response = {'clockid': 0, 'ok': 0, 'msg': ''}
    user = User.objects.get(userid=userid)
    clock = Clock(userid=user, title=title, focustime=focustime,
                  relaxtime=relaxtime, circulate=circulate, blackhouse=blackhouse)
    clock.save()
    if clock.clockid is not None:
        response['clockid'] = clock.clockid
        response['ok'] = 1

    return response


# 删除番茄钟
def deleteClock(userid, clockid):
    response = {'ok': 0, 'msg': ''}
    user = User.objects.get(userid=userid)
    Clock.objects.get(clockid=clockid, userid=user).delete()

    response['ok'] = 1

    return response


# 获取番茄钟列表
def getClockList(userid):
    response = {'data': [], 'ok': 0, 'msg': ''}

    clocks = Clock.objects.filter(userid=userid)

    for clock in clocks:
        obj = {}
        obj['userid'] = clock.userid_id
        obj['clockid'] = clock.clockid
        obj['title'] = clock.title
        obj['focustime'] = clock.focustime
        obj['relaxtime'] = clock.relaxtime
        obj['circulate'] = clock.circulate
        obj['blackhouse'] = clock.blackhouse
        response['data'].append(obj)

    response['ok'] = 1

    return response


# 获取全部计划列表
def getAllPlanList(userid, page, num):
    page = int(page)
    num = int(num)
    response = {'data': [], 'ok': 0, 'msg': ''}

    clocks = getClockList(userid)["data"]
    # clock_num = len(clocks)     # 番茄钟的数量

    todos = todolist.getTodoList(userid)["data"]
    plans = longplan.getLongPlanList(userid)["data"] + todos

    # 长期计划，待办按时间排序
    plans = sorted(plans, key=lambda x: time.mktime(x['endtime'].timetuple()))

    plans = clocks + plans

    max_num = len(plans)
    response['max'] = max_num

    # 页数超过范围
    if (page - 1) * num >= max_num or page < 1:
        return response

    # 取一页内容
    count = 0
    for i in range(num * (page - 1), min(num * page, response['max'])):
        count += 1
        plan = plans[i]
        obj = {'type': 0, 'detail': {}}
        if 'todoid' in plan.keys():
            obj['type'] = 2
        elif 'clockid' in plan.keys():
            obj['type'] = 1
        else:
            obj['type'] = 3
        obj['detail'] = plan
        response['data'].append(obj)

    response['num'] = count
    response['ok'] = 1
    return response


# 获取番茄钟/长期计划具体信息
def getClockDetail(userid, type, clockid):
    '''
    type: clock:番茄钟  plan:长期计划
    返回值type：0:番茄钟,1:长期计划
    '''
    response = {'ok': 0, 'msg': ''}
    user = User.objects.get(userid=userid)

    if type == 'clock':
        clock = Clock.objects.get(userid=user, clockid=clockid)
        response['userid'] = clock.userid_id
        response['clockid'] = clock.clockid
        response['title'] = clock.title
        response['focustime'] = clock.focustime
        response['relaxtime'] = clock.relaxtime
        response['circulate'] = clock.circulate
        response['blackhouse'] = clock.blackhouse
        response['type'] = 0
    elif type == 'plan':
        plan = Longplan.objects.get(userid=user, planid=clockid)
        response['userid'] = plan.userid_id
        response['clockid'] = plan.planid
        response['title'] = plan.title
        response['focustime'] = plan.focustime
        response['relaxtime'] = plan.relaxtime
        response['circulate'] = plan.circulate
        response['blackhouse'] = plan.blackhouse
        response['type'] = 1

    response['ok'] = 1
    return response
