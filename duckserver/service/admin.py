from DuckFarm import settings
from duckserver.models import User, WorkImg
from duckserver.models import Work
from duckserver.models import Event
from duckserver.models import Tagwork
from duckserver.models import Community
from duckserver.models import Tagreflect
from django.core.paginator import Paginator


def report(eventtype, eventid, reason=''):
    '''
    eventtype: 0:用户举报   1：作品举报    2：圈子举报  3：圈子申请
    '''
    eventid = int(eventid)
    eventtype = int(eventtype)
    response = {'ok': 0, 'msg': ''}
    if eventtype not in range(4):
        response['msg'] = 'eventtype错误'
        return response

    # 验证id是否合法
    if eventtype == 0:
        User.objects.get(userid=eventid)
    elif eventtype == 1:
        Work.objects.get(workid=eventid)
    else:
        Community.objects.get(groupid=eventid)

    event = Event.objects.get_or_create(
        eventtype=eventtype, eventid=eventid, status=0)[0]

    event.reason = reason
    event.save()
    response['ok'] = 1
    response['eid'] = event.eid
    return response


# 管理员处理
def adminHandle(userid, eid, result):
    '''
    result: 1:处理  -1：驳回
    eventtype: 0:用户举报   1：作品举报    2：圈子举报  3：圈子申请
    '''
    result = int(result)
    response = {'ok': 0, 'msg': ''}

    user = User.objects.get(userid=userid)
    if user.isadministator == 0:
        response['msg'] = '无权限进行本操作！'
        return response
    event = Event.objects.get(eid=eid)
    event.status = result
    event.save()

    eventtype = event.eventtype
    eventid = event.eventid
    # 进行相应处理
    if result == 1:
        if eventtype == 0:
            user = User.objects.get(userid=eventid)
            user.status = 1
            user.save()
            response['msg'] = '该用户已封禁'
        elif eventtype == 1:
            work = Work.objects.get(workid=eventid)
            work.status = 1
            work.save()
            response['msg'] = '该帖已被删除'
        elif eventtype == 2:
            group = Community.objects.get(groupid=eventid)
            group.status = 1
            group.save()
            response['msg'] = '该圈子已封禁'
        elif eventtype == 3:
            group = Community.objects.get(groupid=eventid)
            group.status = 0
            group.save()
            response['msg'] = '圈子审核通过'
    else:
        if eventtype == 3:
            group = Community.objects.get(groupid=eventid)
            group.status = 1
            group.save()
            response['msg'] = '圈子审核未通过'
    response['ok'] = 1
    return response


# 管理员审核事件
# def adminGetEvents(userid):
#     response = {'ok': 0, 'data': [], 'msg': ''}
#     admin = User.objects.get(userid=userid)
#
#     if admin.isadministator == 0:
#         response['msg'] = '无权限进行本操作！'
#         return response
#
#     eventList = Event.objects.filter(status=0).order_by("-eid")
#     for event in eventList:
#         obj = {}
#         eventtype = event.eventtype
#         eventid = event.eventid
#         obj['eventtype'] = eventtype
#         obj['eventid'] = eventid
#         obj['eid'] = event.eid
#         obj['status'] = event.status
#         # 消息概要
#         if eventtype == 0:
#             username = User.objects.get(userid=eventid).username
#             obj['msg'] = '用户“%s”已被举报' % username
#         elif eventtype == 1:
#             work = Work.objects.get(workid=eventid).text
#             obj['msg'] = '帖子“%.10s...”已被举报' % work
#         elif eventtype == 2:
#             groupname = Community.objects.get(groupid=eventid).groupname
#             obj['msg'] = '圈子“%s”已被举报' % groupname
#         elif eventtype == 3:
#             group = Community.objects.get(groupid=eventid)
#             groupowner = group.userid.username
#             obj['msg'] = '%s 申请创建圈子“%s”' % (groupowner, group.groupname)
#
#         response['data'].append(obj)
#     response['ok'] = 1
#     return response

# 前端get事件
'''
        {
          eid: 1,
          groupname: "新圈子1",
          description: "新圈子1的描述",
          image:
            "https://c-ssl.duitang.com/uploads/item/202003/10/20200310125852_acnpz.png",
          notice: "新圈子1的公告",
          reason: "圈子1的理由",
          username: "申请用户",
          userid: 1,
        }
'''
def adminGetEvents(userid, page, type):
    page = int(page)+1
    type = int(type)
    response = {'ok': 0, 'data': [], 'msg': ''}
    admin = User.objects.get(userid=userid)
    if admin.isadministator == 0:
        response['msg'] = '无权限进行本操作！'
        return response
    if type == 3:
        eventList = Event.objects.filter(status=0).filter(eventtype=3).order_by('eid')

        paginator = Paginator(eventList, 10)
        event_page = paginator.page(page)  # 分页 从1开始
        response['max'] = paginator.count  # 总条数


        for event in event_page:
            obj = {}
            eventtype = event.eventtype
            eventid = event.eventid
            group = Community.objects.get(groupid=eventid)
            obj['eventtype'] = eventtype
            obj['eventid'] = eventid
            obj['eid'] = event.eid
            obj['status'] = event.status
            obj['groupname'] = group.groupname
            obj['description'] = group.description
            obj['notice'] = group.announcement
            obj['reason'] = event.reason
            obj['username'] = group.userid.username
            obj['userid'] = group.userid.userid
            obj['image'] = settings.ROOT_URL + group.backgroundimage.url
            response['data'].append(obj)

    if type == 1:
        eventList = Event.objects.filter(status=0).filter(eventtype=1).order_by('eid')

        paginator = Paginator(eventList, 10)
        event_page = paginator.page(page)  # 分页 从1开始
        response['max'] = paginator.count  # 总条数

        for event in event_page:
            obj = {}
            eventtype = event.eventtype
            eventid = event.eventid
            work = Work.objects.get(workid=eventid)
            obj['eventtype'] = eventtype
            obj['eventid'] = eventid
            obj['eid'] = event.eid
            obj['status'] = event.status
            obj['workid'] = work.workid
            obj['title'] = work.title
            obj['content'] = work.text
            imgs = WorkImg.objects.filter(workid=work)
            images = []
            for img in imgs:
                images.append(settings.ROOT_URL + img.image.url)
            obj['userImage'] = images
            response['data'].append(obj)
    response['ok'] = 1
    return response