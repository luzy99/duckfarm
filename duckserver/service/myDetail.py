from duckserver.models import User,Community,Comment,Grouprelation,Likerelation,Follow,Work
from django.conf import settings
import time
'''
        {
          id: 1,
          groupId: 112221,
          groupName: "刘佬的小弟们",
          groupImg: "https://img.yzcdn.cn/vant/cat.jpeg",
          groupDescribe: "这里是ldn大佬的小弟们",
          worknums:100,
          membernums:24
        },
'''


def my_community(userid):
    response = {'ok': 0, 'msg': ''}
    user=User.objects.get(userid=userid)
    groups=Grouprelation.objects.filter(userid=userid).order_by('-createtime')
    id=0
    data=[]
    for group in groups:
        id+=1
        groupid=group.groupid
        group=Community.objects.get(groupid=groupid.groupid)
        groupname=group.groupname
        groupImg=settings.ROOT_URL + group.backgroundimage.url
        groupdesc=group.description
        #获取成员数量
        members=Grouprelation.objects.filter(groupid=groupid)
        membernums=len(members)
        #获取作品数量
        works=Work.objects.filter(groupid=groupid)
        worknums=len(works)
        data.append({
            'id':id,
            'groupId':groupid.groupid,
            'groupName':"%.18s" % groupname,
            'groupImg':groupImg,
            'groupDescribe':"%.18s" % groupdesc,
            'worknums':worknums,
            'membernums':membernums
        })

    response['groupList'] = data

    response['ok'] = 1
    return response

def my_wallet(userid):
    response = {'ok': 0, 'msg': ''}
    user=User.objects.get(userid=userid)
    response['power']=int(user.power)
    response['eggs']=user.credit
    response['ok'] = 1
    return response

'''
{id: 1, 
userId:112221,
userName:"刘佬",
userImg:"https://img.yzcdn.cn/vant/cat.jpeg",
worknums:16,
fansnum:49,
isFollow:true},
'''

# 将前端的userdescribe改成粉丝数量和作品数量

def my_fans(userid):
    response = {'ok': 0, 'msg': ''}
    user=User.objects.get(userid=userid)
    id=0
    fans=[]
    followers=Follow.objects.filter(userid=userid).order_by('-createtime')
    for follower in followers:
        id+=1
        data = {}
        data['id']=id
        data['userId'] = follower.follower.userid
        data['userName'] = "%.18s" % follower.follower.username
        data['userImg'] = settings.ROOT_URL + follower.follower.headImage.url
        works=Work.objects.filter(userid=follower.follower)
        data['worknums']=len(works)
        data['fansnum']=follower.follower.fansNum
        follow_relation=Follow.objects.filter(userid=follower.follower,follower=userid)
        if len(follow_relation)==0:
            data['isFollow']=False
        else:
            data['isFollow']=True
        fans.append(data)
    response['data']=fans
    response['ok'] = 1
    return response

#我的关注
def my_follows(userid):
    response = {'ok': 0, 'msg': ''}
    user=User.objects.get(userid=userid)
    id=0
    follow=[]
    carers=Follow.objects.filter(follower=userid).order_by('-createtime')
    for carer in carers:
        id+=1
        data = {}
        data['id']=id
        data['userId'] = carer.userid.userid
        data['userName'] = "%.18s" % carer.userid.username
        data['userImg'] = settings.ROOT_URL + carer.userid.headImage.url
        data['fansnum']=carer.userid.fansNum
        works=Work.objects.filter(userid=carer.userid)
        data['worknums']=len(works)
        data['isFollow']=True
        follow.append(data)
    response['data']=follow
    response['ok'] = 1
    return response

'''
    id: 1,
    userId: 112221,
    userName: "谈踩踩",
    userImg: "https://img.yzcdn.cn/vant/cat.jpeg",
    releaseTime: "2019-12-05",
    isFollow: false,
    content: "刘佬强啊",
    likenums:2333,
    commentnums:666
'''
def my_likeworks(userid):
    response = {'ok': 0, 'msg': ''}
    user=User.objects.get(userid=userid)
    id=0
    worklist=[]
    likerelations=Likerelation.objects.filter(userid=userid).order_by('-createtime')
    for likerelation in likerelations:
        id+=1
        work=Work.objects.get(workid=likerelation.workid.workid)
        data = {}
        author=User.objects.get(userid=work.userid.userid)
        data['id']=id
        data['userId']=author.userid
        data['workId']=work.workid
        data['userName']="%.18s" % author.username
        data['userImg']=settings.ROOT_URL + author.headImage.url
        data['releaseTime']=work.releasetime.strftime('%Y-%m-%d %H:%M:%S')
        follow_relation=Follow.objects.filter(userid=author.userid,follower=userid)
        if len(follow_relation)==0:
            data['isFollow']=False
        else:
            data['isFollow']=True
        data['content']= "%.18s" % work.text
        data['likenums']=work.attitudenum
        data['commentnums']=work.commentnum
        worklist.append(data)
    response['data']=worklist
    response['ok'] = 1
    return response


def my_works(userid):
    response = {'ok': 0, 'msg': ''}
    user=User.objects.get(userid=userid)
    id=0
    worklist=[]
    works=Work.objects.filter(userid=userid).order_by('-releasetime')
    response['userId']=user.userid
    response['userName']="%.18s" %user.username
    response['userImg']=settings.ROOT_URL + user.headImage.url
    for work in works:
        id+=1
        data = {}
        data['id']=id
        data['workId']=work.workid
        data['releaseTime']=work.releasetime.strftime('%Y-%m-%d %H:%M:%S')
        data['content']= "%.18s" %work.text
        data['likenums']=work.attitudenum
        data['commentnums']=work.commentnum
        worklist.append(data)
    response['data']=worklist
    response['ok'] = 1
    return response


def my_messages(userid):
    response = {'ok': 0, 'msg': ''}
    user=User.objects.get(userid=userid)
    id=0
    msglist=[]
    userName=user.username
    # 关注
    followmsgs=Follow.objects.filter(userid=userid)
    for followmsg in followmsgs:
        id+=1
        data={}
        data['id']=id
        data['userId']=followmsg.follower.userid
        followUser=User.objects.get(userid=followmsg.follower.userid)
        data['userName']="%.18s" % followUser.username
        data['userImg']=settings.ROOT_URL + followUser.headImage.url
        data['msgtime']=followmsg.createtime.strftime('%Y-%m-%d %H:%M:%S')
        data['msgtype']=0
        msglist.append(data)
    # 点赞
    myworks=Work.objects.filter(userid=userid)
    likerelations=Likerelation.objects.none()
    for mywork in myworks:
        likerelations=likerelations | Likerelation.objects.filter(workid=mywork.workid)
    for likerelation in likerelations:
        id+=1
        data={}
        data['id']=id
        data['userId']=likerelation.userid.userid
        data['userName']="%.18s" % likerelation.userid.username
        data['userImg']=settings.ROOT_URL +likerelation.userid.headImage.url
        data['msgtime']=likerelation.createtime.strftime('%Y-%m-%d %H:%M:%S')
        data['msgtype']=1
        work=Work.objects.get(workid=likerelation.workid.workid)
        data['workid']=work.workid
        data['worktitle']="%.18s" %work.title
        msglist.append(data)
    #评论
    comments=Comment.objects.filter(parentuserid=userid)
    for comment in comments:
        cmuser=User.objects.get(userid=comment.userid.userid)
        id+=1
        data={}
        data['id']=id
        data['userId']=cmuser.userid
        data['userName']="%.18s" %cmuser.username
        data['userImg']=settings.ROOT_URL + cmuser.headImage.url
        data['msgtime']=comment.releasetime.strftime('%Y-%m-%d %H:%M:%S')
        if comment.commentparentid[0]== '@':
            data['msgtype']=2
            work=Work.objects.get(workid=comment.commentparentid[1:])
            data['workid']=work.workid
            data['worktitle']="%.18s" %work.title
        else:
            data['msgtype']=3
            data['mycomment']=Comment.objects.get(commentid=comment.commentparentid).content
        
        data['commentid']=comment.commentid
        data['comment']="%.18s" % comment.content    
        msglist.append(data)

    # 时间逆序排序
    def sort_by_time(x):
        timeArray = time.strptime(x['msgtime'], "%Y-%m-%d %H:%M:%S")
        timestamp = time.mktime(timeArray)
        return timestamp
    msglist = sorted(msglist, key=sort_by_time, reverse=True)
    response['data']=msglist
    response['ok'] = 1
    return response
    