import datetime

from django.db.models import F

from duckserver.models import Room
from duckserver.models import User

# 加入自习室
def joinRoom(roomid, userid):

    response = {'roomid': 0, 'ok': 0, 'msg': ''}
    exist_room = Room.objects.filter(roomid=roomid).first()
    if len(Room.objects.filter(roomid=roomid)) == 0:
        response['msg'] = "自习室不存在"
        return response
    if (exist_room.usercount+1) > exist_room.roommaxnum:
        response['msg'] = '自习室人员已满'
        response['usercount'] = exist_room.usercount
    # elif exist_room.isprivate==1:
    #     response['msg'] = '自习室私有'
    #     response['usercount'] = exist_room.usercount
    else:
        room = Room(roomid=roomid, roomname=exist_room.roomname,
                    usercount=exist_room.usercount+1, roommaxnum=exist_room.roommaxnum,
                    userid=User.objects.get(userid=userid), isprivate=exist_room.isprivate,
                    jointime=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        room.save()
        Room.objects.filter(roomid=roomid).update(usercount=exist_room.usercount+1)
        response["roomid"] = room.roomid
        response["ok"] = 1
        response["msg"] = ''
        response['usercount'] = exist_room.usercount+1
    return response


# 创建自习室
def createRoom(userid,isPrivate,roomMaxNum,roomname):
    response = {'roomid': 0, 'ok': 0, 'msg': ''}
    roomtemp = Room.objects.order_by('-roomid').first()
    print(roomtemp)
    id = 1
    if roomtemp != None:
        id = roomtemp.roomid+1

    user = User.objects.get(userid=userid)
    room = Room(roomid=id, roomname=roomname, usercount=1, roommaxnum=roomMaxNum,
                userid=user, isprivate=isPrivate, jointime=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    room.save()
    response['roomid'] = room.roomid
    response["ok"] = 1
    response["msg"] = ''
    return response


# 获取自习室列表(随机10个)
def getRoomList():
    response = {'data': [], 'ok': 0, 'msg': ''}
    roomlist = Room.objects.filter(isprivate=0).filter(
        usercount__lt=F('roommaxnum')).order_by('?').values('roomid')
    # roomlist = roomlist.distinct()[:10]
    data = []
    counter = 0
    distinct_list=[]
    for i in range(0, len(roomlist)):
        if roomlist[i] in distinct_list:
            continue
        else:
            distinct_list.append(roomlist[i])
            counter+=1
        if counter == 10:
            break
    roomlist = distinct_list
    # print(distinct_list)

    if len(roomlist) == 0:
        response['msg'] = "当前没有可加入的自习室"
        response['ok'] = 1
        response['data'] = data
        return response

    for room in roomlist:
        roomdata = {}
        roomdata['roomid'] = room['roomid']
        roomtemp = Room.objects.filter(roomid=room['roomid']).first()
        roomdata['roomname'] = roomtemp.roomname
        roomdata['userCount'] = roomtemp.usercount
        roomdata['roomMaxNum'] = roomtemp.roommaxnum
        data.append(roomdata)
    response['data'] = data
    response["ok"] = 1
    response["msg"] = ''
    return response
