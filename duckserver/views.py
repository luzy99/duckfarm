from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.core.files.base import ContentFile
from django.core import serializers
from django.http import JsonResponse
from duckserver.service import user
from duckserver.service import room
from duckserver.service import todolist
import datetime
from duckserver.service import tomatoClock
from duckserver.service import record
from duckserver.service import longplan
from duckserver.service import work
from duckserver.service import admin
import json
from duckserver.service import community
from duckserver.service import chart
from duckserver.service import mail
from duckserver.service import myDetail


# Create your views here.
@require_http_methods(["GET"])
def getuser(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        response = user.getuser(userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 用户注册
@require_http_methods(["GET"])
def signup(request):
    response = {}
    try:
        username = request.GET.get('username')
        pwd = request.GET.get('password')
        email = request.GET.get('email')
        verifycode = request.GET.get('verifycode')
        response = user.signup(username, pwd, email, verifycode)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 用户登录
@require_http_methods(["GET"])
def login(request):
    response = {}
    try:
        username = request.GET.get('username')
        pwd = request.GET.get('password')
        response = user.login(username, pwd)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 获得用户信息
@require_http_methods(["GET"])
def getUserInfo(request):
    response = {}
    try:
        uid = request.GET.get('userid')
        myid = request.GET.get('myid', uid)
        response = user.getUserInfo(uid, myid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 修改用户信息 userid, username, password, imgList, sex, type
@require_http_methods(["POST"])
def updateUserInfo(request):
    response = {}
    try:
        uid = request.POST.get('userid')
        username = request.POST.get('username')
        password = request.POST.get('password')
        imgList = request.FILES.getlist('imgList')
        sex = request.POST.get('sex')
        type = request.POST.get('type')
        response = user.updateUserInfo(uid, username, password, imgList, sex, type)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 加入自习室
@require_http_methods(["GET"])
def joinRoom(request):
    response = {}
    try:
        roomid = request.GET.get('roomid')
        uid = request.GET.get('userid')
        response = room.joinRoom(roomid, uid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 新建自习室
@require_http_methods(["GET"])
def createRoom(request):
    response = {}
    try:
        uid = request.GET.get('userid')
        isPrivate = request.GET.get('isPrivate')
        roomMaxNum = request.GET.get('roomMaxNum')
        roomname = request.GET.get('roomname')
        response = room.createRoom(uid, isPrivate, roomMaxNum,roomname)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 获取自习室列表
@require_http_methods(["GET"])
def getRoomList(request):
    response = {}
    try:
        response = room.getRoomList()
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 创建代办清单
@require_http_methods(["GET"])
def createTodo(request):
    response = {}
    try:
        title = request.GET.get('title')
        content = request.GET.get('content')
        userid = request.GET.get('userid')
        endtime = request.GET.get('endtime')
        alarmtime = request.GET.get('alarmtime')

        response = todolist.createTodo(
            title, content, userid, endtime, alarmtime)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 获取待办清单列表
@require_http_methods(["GET"])
def getTodoList(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        response = todolist.getTodoList(userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 新建番茄钟
@require_http_methods(["GET"])
def createClock(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        title = request.GET.get('title')
        focustime = request.GET.get('focustime')
        relaxtime = request.GET.get('relaxtime')
        circulate = request.GET.get('circulate')
        blackhouse = request.GET.get('blackhouse')

        response = tomatoClock.createClock(
            userid, title, focustime, relaxtime, circulate, blackhouse)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 完成待办
@require_http_methods(["GET"])
def finishTodo(request):
    response = {}
    try:
        todoid = request.GET.get('todoid')
        isfinished = request.GET.get('isfinished')
        response = todolist.finishTodo(todoid, isfinished)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 删除番茄钟
@require_http_methods(["GET"])
def deleteClock(request):
    response = {}
    try:
        clockid = request.GET.get('clockid')
        userid = request.GET.get('userid')
        response = tomatoClock.deleteClock(userid, clockid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 删除待办
@require_http_methods(["GET"])
def deleteTodo(request):
    response = {}
    try:
        todoid = request.GET.get('todoid')
        userid = request.GET.get('userid')
        response = todolist.deleteTodo(userid, todoid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 获取番茄钟列表
@require_http_methods(["GET"])
def getClockList(request):
    response = {}
    try:
        userid = request.GET.get('userid')

        response = tomatoClock.getClockList(userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 新建学习记录
@require_http_methods(["GET"])
def createStudyRecord(request):
    response = {}
    # try:
    target = request.GET.get('target')
    userid = request.GET.get('userid')
    starttime = request.GET.get('starttime')
    runtime = request.GET.get('runtime')
    iscompleted = request.GET.get('iscompleted')
    failreason = request.GET.get('failreason')
    type = int(request.GET.get('type'))
    planid = request.GET.get('planid')
    if planid is not None:
        planid = int(planid)
    response = record.createStudyRecord(target,userid,starttime,runtime,iscompleted,failreason,type,planid)
    # except Exception as e:
    #     response['msg'] = str(e)
    #     response['ok'] = 0
    return JsonResponse(response)


# 获取自习室学习情况
@require_http_methods(["GET"])
def getRoomSituation(request):
    response = {}
    try:
        roomid = request.GET.get('roomid')
        page = request.GET.get('page')
        response = record.getRoomSituation(roomid,page)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 获取排行榜
@require_http_methods(["GET"])
def getStudyRank(request):
    response = {}
    try:
        page = request.GET.get('page')
        response = record.getStudyRank(page)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

def timestamp2string(timeStamp):
    try:
        d = datetime.datetime.fromtimestamp(int(timeStamp))
        str1 = d.strftime("%Y-%m-%d %H:%M:%S")
        # 2015-08-28 16:43:37'
        return str1
    except Exception as e:
        print(e)
        return ''


# 新建长期计划
@require_http_methods(["GET"])
def createLongPlan(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        title = request.GET.get('title')
        targettime = request.GET.get('targettime')
        endtime = request.GET.get('endtime')
        alarmtime = request.GET.get('alarmtime')
        content = request.GET.get('content')
        focustime = request.GET.get('focustime')
        relaxtime = request.GET.get('relaxtime')
        circulate = request.GET.get('circulate')
        blackhouse = request.GET.get('blackhouse')

        response = longplan.createLongPlan(
            userid, title, targettime, endtime, alarmtime, content, focustime, relaxtime, circulate, blackhouse)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 获取长期计划列表
@require_http_methods(["GET"])
def getLongPlanList(request):
    response = {}
    try:
        userid = request.GET.get('userid')

        response = longplan.getLongPlanList(userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 删除长期计划列表
@require_http_methods(["GET"])
def deleteLongPlan(request):
    response = {}
    try:
        planid = request.GET.get('planid')
        userid = request.GET.get('userid')
        response = longplan.deleteLongPlan(userid, planid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 获取所有计划列表
@require_http_methods(["GET"])
def getAllPlanList(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        page = request.GET.get('page', 1)
        num = request.GET.get('num', 10)

        response = tomatoClock.getAllPlanList(userid, page, num)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)



# 获取粉丝、关注列表
@require_http_methods(["GET"])
def getFansCareList(request):
    response = {}
    try:
        userid = request.GET.get('userid')

        response = user.getFansCareList(userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 关注、取消
@require_http_methods(["GET"])
def follow(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        careid = request.GET.get('careid')
        response = user.follow(userid, careid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 添加评论
@require_http_methods(["GET"])
def addComment(request):
    response = {}
    try:
        commentparentid = request.GET.get('commentparentid')
        userid = request.GET.get('userid')
        content = request.GET.get('content')
        rootknot = request.GET.get('rootknot')
        response = user.addComment(commentparentid, userid, content, rootknot)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 点赞
@require_http_methods(["GET"])
def thumbUp(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        workid = request.GET.get('workid')

        response = user.thumbUp(userid, workid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 购买 purchaseWork  userid, workid
@require_http_methods(["GET"])
def purchaseWork(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        workid = request.GET.get('workid')

        response = user.purchaseWork(userid, workid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)



# 发帖
@require_http_methods(["POST"])
def postWork(request):
    response = {}
    try:
        userid = request.POST.get('userid')
        text = request.POST.get('text')
        title = request.POST.get('title')
        getPaid = request.POST.get('getPaid')
        groupid = request.POST.get('groupid')
        tag = request.POST.getlist('tag')
        imgList = request.FILES.getlist('imgList')
        response = work.postWork(userid, title, text, getPaid, groupid, tag, imgList)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 创建圈子
@require_http_methods(["POST"])
def createGroup(request):
    response = {}
    try:
        userid = request.POST.get('userid')
        groupname = request.POST.get('groupname')
        description = request.POST.get('description')
        announcement = request.POST.get('announcement')
        reason = request.POST.get('reason')
        backgroundimage = request.FILES['backgroundimage']
        response = community.createGroup(userid, groupname, description, backgroundimage, announcement, reason)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 删帖
@require_http_methods(["GET"])
def deleteWork(request):
    response = {}
    try:
        workid = request.GET.get('workid')
        response = work.deleteWork(workid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 获取圈子信息
@require_http_methods(["GET"])
def getGroupMessage(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        groupid= request.GET.get('groupid')

        response = community.getGroupMessage(groupid,userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 获取圈子列表
@require_http_methods(["GET"])
def getGroupList(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        page = request.GET.get('page', 1)
        num = request.GET.get('num', 5)
        num = int(num)
        response = community.getGroupList(userid, page, num)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 加入圈子
@require_http_methods(["GET"])
def joinGroup(request):
    response = {}
    try:
        groupid= request.GET.get('groupid')
        userid = request.GET.get('userid')

        response = community.joinGroup(groupid,userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 退出圈子
@require_http_methods(["GET"])
def exitGroup(request):
    response = {}
    try:
        groupid= request.GET.get('groupid')
        userid = request.GET.get('userid')

        response = community.exitGroup(groupid,userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 获取圈子成员列表
@require_http_methods(["GET"])
def getGroupMember(request):
    response = {}
    try:
        groupid = request.GET.get('groupid')
        userid = request.GET.get('userid')
        page = request.GET.get('page', 1)
        num = request.GET.get('num', 10)

        response = community.getGroupMember(groupid, userid, num, page)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 管理员处理
@require_http_methods(["GET"])
def adminHandle(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        eid = request.GET.get('eid')
        result = request.GET.get('result')
        response = admin.adminHandle(userid, eid, result)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 管理员处理
@require_http_methods(["GET"])
def adminGetEvents(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        page = request.GET.get('page')
        type = request.GET.get('type')
        response = admin.adminGetEvents(userid, page, type)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 举报
@require_http_methods(["GET"])
def report(request):
    response = {}
    try:
        eventtype = request.GET.get('eventtype')
        eventid = request.GET.get('eventid')
        response = admin.report(eventtype, eventid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 获取用户专注时间分布
@require_http_methods(["GET"])
def getFocusDistribution(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        response = chart.getFocusDistribution(userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 更换用户头像
@require_http_methods(["POST"])
def changeUserHeadImg(request):
    response = {}
    try:
        userid = request.POST.get('userid')
        img = request.FILES['img']
        response = user.changeUserHeadImg(userid, img)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 获取验证码
@require_http_methods(["GET"])
def getVerifyCode(request):
    response = {}
    try:
        email = request.GET.get('email')
        response = mail.send_verify_mail(email)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)


# 获取用户近一月的每天学习时长
@require_http_methods(["GET"])
def getLineChart(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        response = chart.getLineChart(userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 获取番茄钟/长期计划详情
@require_http_methods(["GET"])
def getClockDetail(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        type = request.GET.get('type')
        clockid = request.GET.get('clockid')
        response = tomatoClock.getClockDetail(userid, type, clockid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 获取帖子详情
@require_http_methods(["GET"])
def getWorkInfo(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        workid = request.GET.get('workid')
        response = work.getWorkInfo(userid, workid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 获取标签详情
@require_http_methods(["GET"])
def getTagInfo(request):
    response = {}
    try:
        tagid = request.GET.get('tagid')
        response = work.getTagInfo(tagid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 获取帖子列表
@require_http_methods(["GET"])
def getWorkList(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        type = request.GET.get('type')
        page = request.GET.get('page', 1)
        num = request.GET.get('num', 5)
        para = request.GET.get('para', 0)  # 表示 tagid 或 groupid
        if para == "":
            para = 0
        # groupid = request.GET.get('groupid', 0)
        # tagid = request.GET.get('tagid', 0)
        response = work.getWorkList(userid, type, page, num, para, para)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 获取用户近一月失败原因
@require_http_methods(["GET"])
def getFailReasonChart(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        response = chart.getFailReasonChart(userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 获取用户月度学习时间
@require_http_methods(["GET"])
def getWorkTime(request):
    respose={}
    try:
        userid = request.GET.get('userid')
        response = chart.getWorkTime(userid)
        # print(response['data'])
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

#搜索
@require_http_methods(["GET"])
def search(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        type = request.GET.get('type')
        page = request.GET.get('page', 1)
        num = request.GET.get('num', 5)
        keyword = request.GET.get('keyword')
        response = work.search(userid,keyword,type,page, num)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

#发现
@require_http_methods(["GET"])
def discover(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        type = request.GET.get('type')
        page = request.GET.get('page', 1)
        num = request.GET.get('num', 5)
        response = work.discover(userid,type,page,num)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 我的页面子页面--圈子页面
@require_http_methods(["GET"])
def my_community(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        response = myDetail.my_community(userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 我的页面子页面--钱包页面
@require_http_methods(["GET"])
def my_wallet(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        response = myDetail.my_wallet(userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 我的页面子页面--粉丝页面
@require_http_methods(["GET"])
def my_fans(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        response = myDetail.my_fans(userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

#我的页面子页面--关注页面
@require_http_methods(["GET"])
def my_follows(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        response = myDetail.my_follows(userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 我的页面子页面--作品页面
@require_http_methods(["GET"])
def my_works(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        response = myDetail.my_works(userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

# 我的页面子页面--喜欢的作品页面
@require_http_methods(["GET"])
def my_likeworks(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        response = myDetail.my_likeworks(userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

#我的页面子页面--消息页面
@require_http_methods(["GET"])
def my_messages(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        response = myDetail.my_messages(userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)

#学习详情
@require_http_methods(["GET"])
def getLearningDetail(request):
    response = {}
    try:
        userid = request.GET.get('userid')
        response = chart.getLearningDetail(userid)
    except Exception as e:
        response['msg'] = str(e)
        response['ok'] = 0
    return JsonResponse(response)
