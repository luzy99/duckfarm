from django.apps import AppConfig


class DuckserverConfig(AppConfig):
    name = 'duckserver'
